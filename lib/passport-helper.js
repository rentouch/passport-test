module.exports = function(passport, localStrategy, UserDetails){
	// Serialize - what ever this does?
	passport.serializeUser(function(user, done) {
	  done(null, user);
	});
	 
	passport.deserializeUser(function(user, done) {
	  done(null, user);
	});

	// Strategy
	passport.use(new LocalStrategy(function(username, password, done) {
		console.log("passport1", username)
	  	process.nextTick(function() {
		  	console.log("passport", username)
		    UserDetails.findOne({
		    	'username': username,
		    }, function(err, user){
		    	if (err) return done(err);
		    	if (!user) return done(null, false);
		    	if (user.password != password) return done(null, false);
		    	return done(null, user)
		    })
		});
	}));
}