module.exports = function(express, passport){

	var router = express.Router()

	router.route('/auth').post(function(req, res, next){
		console.log("auth:", req.body)
		passport.authenticate('local', {
			successRedirect: '/loginSuccess',
    		failureRedirect: '/loginFailure'
		})
	})

	router.route('/loginFailure').get(function(req, res, next) {
	  res.send('Failed to authenticate');
	});
	 
	router.route('/loginSuccess').get(function(req, res, next) {
	  res.send('Successfully authenticated');
	});

	return router
}