// Dependecies
var express = require('express'),
	passport = require('passport'),
	bodyParser = require('body-parser');
	LocalStrategy = require('passport-local').Strategy;
	session = require('express-session'),
	mongoose = require('mongoose')

// Modules
var apiRouter = require('./lib/apirouter'),
	mongooseHelper = require('./lib/mongoose-helper'),
	passportHelper = require('./lib/passport-helper')(passport, LocalStrategy, mongooseHelper.UserDetails)

// Server
var app = express()

app.use(bodyParser.json())
app.use(session({
    secret: 'much-secret',
    resave: false,
    saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())

app.use('/api', apiRouter(express, passport))

app.listen(3000)